package com.example.android.slotmachinetest;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private RecyclerView rv1, rv2, rv3;
    private TextView tv1, tv2, tv3;
    private Button btn;

    private static int[] imgs = {
            R.drawable.ic_slot1,
            R.drawable.ic_slot2,
            R.drawable.ic_slot3,
            R.drawable.ic_slot4,
            R.drawable.ic_slot5,
            R.drawable.ic_slot6};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);

        final LinearLayoutManager ll1 = new LinearLayoutManager(this);
        //To make the images load in opposite direction
        ll1.setReverseLayout(true);
        ll1.setStackFromEnd(true);
        //Calculate the offset based on the recyclerView height and imageView height
        ll1.scrollToPositionWithOffset(imgs.length / 2, 60);

        rv1 = (RecyclerView) findViewById(R.id.rv1);
        rv1.setLayoutManager(ll1);
        rv1.setAdapter(new MyAdapter(imgs));
        rv1.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    // To avoid displaying half elements
                    int index = ll1.findLastCompletelyVisibleItemPosition();
                    //To handle an edge case where the recyclerView view landing doesnt result in a position
                    if (index == -1) {
                        index = ll1.findFirstVisibleItemPosition();
                    }
                    ll1.scrollToPositionWithOffset(index, 60);
                    int val = index % imgs.length;
                    getSymbolFrom(val, tv1);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        final LinearLayoutManager ll2 = new LinearLayoutManager(this);
        ll2.setReverseLayout(true);
        ll2.setStackFromEnd(true);
        ll2.scrollToPositionWithOffset(imgs.length / 2, 60);
        rv2 = (RecyclerView) findViewById(R.id.rv2);
        rv2.setLayoutManager(ll2);
        rv2.setAdapter(new MyAdapter(imgs));
        rv2.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    //To avoid displaying half elements
                    int index = ll2.findLastCompletelyVisibleItemPosition();
                    if (index == -1) {
                        index = ll2.findFirstVisibleItemPosition();

                    }
                    ll2.scrollToPositionWithOffset(index, 60);
                    int val = index % imgs.length;
                    getSymbolFrom(val, tv2);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        final LinearLayoutManager ll3 = new LinearLayoutManager(this);
        ll3.setReverseLayout(true);
        ll3.setStackFromEnd(true);
        ll3.scrollToPositionWithOffset(imgs.length / 2, 60);
        rv3 = (RecyclerView) findViewById(R.id.rv3);
        rv3.setLayoutManager(ll3);
        rv3.setAdapter(new MyAdapter(imgs));
        rv3.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    //To avoid displaying half elements
                    int index = ll3.findLastCompletelyVisibleItemPosition();
                    if (index == -1) {
                        index = ll3.findFirstVisibleItemPosition();
                    }
                    ll3.scrollToPositionWithOffset(index, 60);
                    int val = index % imgs.length;
                    getSymbolFrom(val, tv3);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoScrollRV(rv1,30); //Duration to spin the wheel
                autoScrollRV(rv2,40);
                autoScrollRV(rv3,50);
            }
        });

    }

    //dummy method to display the images based on position
    public void getSymbolFrom(int val, TextView textView) {
        switch (val) {
            case 1:
                textView.setText("Orange");
                break;
            case 2:
                textView.setText("Grape");
                break;
            case 3:
                textView.setText("Bell");
                break;
            case 4:
                textView.setText("Bar");
                break;
            case 5:
                textView.setText("Seven");
                break;
            default:
                textView.setText("Cherry");

        }

    }

    public void autoScrollRV(final RecyclerView recyclerView, final int spinTime) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            int count = spinTime;

            @Override
            public void run() {

                if (count >= 0) {
                    recyclerView.smoothScrollBy(0, -400); //Can alter the speed of the reels by changing dy
                    handler.postDelayed(this, 100);
                    count--;
                } else {
                    handler.removeCallbacksAndMessages(this);
                }
            }
        };
        handler.postDelayed(runnable, 100);

    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        //List of images
        private int[] values;

        public MyAdapter(int[] myDataset) {
            values = myDataset;
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView img;
            private View view;

            public ViewHolder(View itemView) {
                super(itemView);
                view = itemView;
                img = (ImageView) view.findViewById(R.id.imageRV);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();

            }
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
            int positionInList = position % values.length;
            holder.img.setImageResource(values[positionInList]);
        }

        @Override
        public int getItemCount() {
            return Integer.MAX_VALUE; //Set the recyclerView size to max
        }
    }

}
